package com.free4inno.knowledgems.openapi;

import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.ConstraintViolationException;

/**
 * Author HUYUZHU.
 * Date 2021/10/24 21:24.
 * 统一异常处理
 */

@ControllerAdvice("com.free4inno.knowledgems.openapi")
public class GlobalExceptionHandle {


    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public Result<?> handle(Exception e) {
        if (e instanceof OpenAPIException) {
            OpenAPIException openAPIException = (OpenAPIException) e;
            return Result.error(openAPIException);
        } else if (e instanceof ConstraintViolationException ||
                e instanceof MissingServletRequestParameterException) {
            return Result.error(ResultEnum.ARGS_ERROR);
        } else {
            // return Result.error(-1, e.getMessage());
            return Result.error(-1, e.toString());
        }
    }

}
