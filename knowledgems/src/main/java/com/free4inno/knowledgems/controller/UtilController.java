package com.free4inno.knowledgems.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.free4inno.knowledgems.dao.AttachmentDAO;
import com.free4inno.knowledgems.domain.Attachment;
import com.free4inno.knowledgems.service.FileDownloadService;
import com.free4inno.knowledgems.service.FileService;
import com.free4inno.knowledgems.constants.UserConstants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.sql.Timestamp;
import java.util.*;
import java.io.File;

/**
 * UtilController.
 */
@Slf4j
@Controller
@RequestMapping("util")
public class UtilController {

    @Autowired
    private AttachmentDAO attachmentDao;

    @Autowired
    private FileService fileService;

    /**
     * 上传图片.
     *
     * @param file
     * @param request
     * @return location(图片要上传的位置).
     * @throws .
     * @Title: uploadImage.
     * @Description: 上传图片.
     */
    @RequestMapping("/uploadImage")
    @ResponseBody
    public Map<String, Object> uploadImage(@RequestParam("file") MultipartFile file,
                                           HttpSession session, HttpServletRequest request) throws Exception {
        log.info(this.getClass().getName() + "----in----" + "上传图片(uploadImage)" + "----" + session.getAttribute(UserConstants.USER_ID));
        // TODO 判断上传是否为图片，富文本编辑器可以以后缀名判断，如后面有需求再加以ImageIO判断的方法

        String path = fileService.uploadFile(file);
        log.info(this.getClass().getName() + "----" + "上传图片路径: " + path + "----" + session.getAttribute(UserConstants.USER_ID));

        // return
        Map<String, Object> ret = new HashMap<>();
        String location = "/util/downloadImage?id=" + path;
        ret.put("location", location);
        log.info(this.getClass().getName() + "----out----" + "图片上传完毕，返回存储地址" + "----" + session.getAttribute(UserConstants.USER_ID));
        return ret;
    }

    @ResponseBody
    @RequestMapping(value = "/downloadImage")
    public void downloadImage(@RequestParam("id") String id, @RequestParam(value = "name", required = false, defaultValue = "") String name,
                              HttpSession session, HttpServletResponse resp) {
        log.info(this.getClass().getName() + "----in----" + "下载图片(downloadImage)" + "----" + session.getAttribute(UserConstants.USER_ID));
        fileService.downloadFile(name, id, true, resp);
        log.info(this.getClass().getName() + "----out----" + "下载图片完毕" + "----" + session.getAttribute(UserConstants.USER_ID));
    }

    @ResponseBody
    @RequestMapping(value = "/uploadAttachment", headers = "content-type=multipart/form-data")
    public Map<String, Object> uploadAttachment(@RequestParam("file") MultipartFile[] file,
                                                HttpSession session, HttpServletRequest request) throws Exception {
        log.info(this.getClass().getName() + "----in----" + "传值到后端上传附件函数(uploadAttachment)" + "----" + session.getAttribute(UserConstants.USER_ID));

        List<String> locations = new ArrayList<String>();
        List<Integer> attachmentIds = new ArrayList<Integer>();
        // 上传多文件
        for(int i=0; i < file.length; i++) {
            String path = fileService.uploadFile(file[i]);
            // 拿到文件路径，代表传递成功
            log.info(this.getClass().getName() + "----" + "上传附件路径: " + path + "----" + session.getAttribute(UserConstants.USER_ID));
            String location = "/util/downloadAttachment?id=" + path;

            //保存附件信息到数据库
            Attachment attachment = new Attachment();
            attachment.setResourceId(null);
            attachment.setCreateTime(new Timestamp(new Date().getTime()));
            attachment.setName(file[i].getOriginalFilename());
            attachment.setUrl(location);
            attachment.setStatus(Attachment.AttachmentEnum.READY);
            Integer attachmentId = attachmentDao.saveAndFlush(attachment).getId();
            log.info(this.getClass().getName() + "----" + "保存附件信息" + "----" + session.getAttribute(UserConstants.USER_ID));

            // return
            locations.add(location);
            attachmentIds.add(attachmentId);
            log.info(this.getClass().getName() + "----out----" + "附件上传完毕，返回存储地址" + "----" + session.getAttribute(UserConstants.USER_ID));
        }
        Map<String, Object> ret = new HashMap<>();
        ret.put("location", locations);
        ret.put("attachmentId", attachmentIds);
//        System.out.println(ret);
        log.info(this.getClass().getName() + "----finish----" + ret + "----" + session.getAttribute(UserConstants.USER_ID));

        return ret;
    }

    @ResponseBody
    @RequestMapping(value = "/downloadAttachment")
    public void downloadAttachment(@RequestParam("id") String id, @RequestParam(value = "name", required = false, defaultValue = "") String name,
                                   HttpSession session, HttpServletResponse resp) {
        log.info(this.getClass().getName() + "----in----" + "下载附件(downloadAttachment)" + "----" + session.getAttribute(UserConstants.USER_ID));
        fileService.downloadFile(name, id, false, resp);
        log.info(this.getClass().getName() + "----out----" + "下载附件完毕" + "----" + session.getAttribute(UserConstants.USER_ID));
    }

    @ResponseBody
    @RequestMapping("/deleteAttachment")
    public Map<String, Object> deleteAttachment(@RequestParam("saveUrl") String saveUrl,
                                                HttpSession session, HttpServletRequest request) throws Exception {
        log.info(this.getClass().getName() + "----in----" + "删除附件(deleteAttachment)" + "----" + session.getAttribute(UserConstants.USER_ID));
        // 清除数据库中的附件数据，并删除相应文件
        try {
            attachmentDao.deleteByUrl(saveUrl);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Map<String, Object> ret = new HashMap<>();
        if (fileService.deleteFile(saveUrl)) {
            ret.put("result", "SUCCESS");
            log.info(this.getClass().getName() + "----out----" + "删除附件成功" + "----" + session.getAttribute(UserConstants.USER_ID));
        } else {
            ret.put("result", "ERROR");
            log.info(this.getClass().getName() + "----in----" + "删除附件错误" + "----" + session.getAttribute(UserConstants.USER_ID));
        }
        return ret;
    }
}
