package com.free4inno.knowledgems.service;

import com.free4inno.knowledgems.dao.GroupInfoDAO;
import com.free4inno.knowledgems.dao.SystemManageDAO;
import com.free4inno.knowledgems.dao.UserDAO;
import com.free4inno.knowledgems.dao.UserGroupDAO;
import com.free4inno.knowledgems.domain.GroupInfo;
import com.free4inno.knowledgems.domain.SystemManage;
import com.free4inno.knowledgems.domain.User;
import com.free4inno.knowledgems.domain.UserGroup;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Author HUYUZHU.
 * Date 2021/3/13 1:55.
 */

@Slf4j
@Service
public class UserService {

    @Autowired
    private UserDAO userDao;

    @Autowired
    private GroupInfoDAO groupInfoDao;

    @Autowired
    private UserGroupDAO userGroupDao;

    @Autowired
    private SystemManageDAO systemManageDao;

    /**
     * 获取系统设置中的用户默认密码
     */
    public String getDefaultPassword () {
        return systemManageDao.findAllByVariable("default_password").orElse(new SystemManage()).getValue();
    }

    /**
     * 判断是否存在该邮箱/手机号的用户.
     *
     * @param mail
     * @param tel
     */
    public boolean userExist(String mail, String tel) {
        log.info(this.getClass().getName() + "----in----" + "判断是否存在该邮箱/手机号的用户" + "----");
        log.info("邮箱："+ mail + "----" + "手机号：" + tel + "----");
        if (userDao.findAllByMail(mail).isPresent() || userDao.findAllByAccount(tel).isPresent()) {
            log.info(this.getClass().getName() + "----out----" + "用户存在，返回true" + "----");
            return true;
        } else {
            log.info(this.getClass().getName() + "----out----" + "用户不存在，返回false" + "----");
            return false;
        }
    }

    /**
     * 返回登录用户所在的所有群组的信息.
     *
     * @param userId
     * @return groupInfoList
     */
    public List<GroupInfo> userGroupInfosAll(Integer userId) {
        log.info(this.getClass().getName() + "----in----" + "查询用户所在的所有群组的信息" + "----");
        log.info("用户ID:" + userId + "----");
        List<UserGroup> userGroups = userGroupDao.findByUserId(userId);
        List<GroupInfo> groupInfoList = new ArrayList<>();
        if (userGroups.size() != 0 && userGroups != null) {
            userGroups.forEach(item -> {
                int groupId = item.getGroupId();
                GroupInfo groupInfo = groupInfoDao.findById(groupId).orElse(new GroupInfo());
                groupInfoList.add(groupInfo);
            });
        }
        log.info(this.getClass().getName() + "----out----" + "返回查询到的群组信息列表" + "----");
        return groupInfoList;
    }

    /**
     * 返回用户是否为超级管理员.
     *
     * @param userId
     * @return boolean
     */
    public boolean isSuperUser(String userId) {
        log.info(this.getClass().getName() + "----in----" + "判断用户是否为超级管理员" + "----");
        log.info("用户ID:" + userId + "----");
        User user = userDao.findById(Integer.parseInt(userId)).orElse(new User());
        if (user.getRoleId() == 1) {
            log.info(this.getClass().getName() + "----out----" + "该用户是超管" + "----");
            return true;
        } else {
            log.info(this.getClass().getName() + "----out----" + "该用户非超管" + "----");
            return false;
        }
    }

    public boolean isContentUser(String userId) {
        log.info(this.getClass().getName() + "----in----" + "判断用户是否为内容管理员" + "----");
        log.info("用户ID:" + userId + "----");
        User user = userDao.findById(Integer.parseInt(userId)).orElse(new User());
        if (user.getRoleId() == 3) {
            log.info(this.getClass().getName() + "----out----" + "该用户是内容管理员" + "----");
            return true;
        } else {
            log.info(this.getClass().getName() + "----out----" + "该用户非内容管理员" + "----");
            return false;
        }
    }
}
