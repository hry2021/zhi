package com.free4inno.knowledgems.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import java.sql.Timestamp;


/**
 * Author HaoYi.
 * Date 2020/9/11.
 */
@Entity
@Table(name = "resource")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Resource {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id; //主键

    @Column(name = "crop_id")
    private Integer cropId; //CropId

    @Column(name = "user_id")
    private Integer userId; //创作者id

    @Column(name = "create_time")
    private Timestamp createTime; //资源创建时间

    @Column(name = "edit_time")
    private Timestamp editTime; //资源修改时间

    @Column(name = "title", columnDefinition = "TEXT")
    private String title; //标题

    @Column(name = "text", columnDefinition = "LONGTEXT")
    private String text; //正文

    @Column(name = "attachment", columnDefinition = "TEXT")
    private String attachment; //附件字段，用来保存附件的url地址（一个或者多个），以json格式保存

    @Column(name = "superior")
    private Integer superior; //资源等级，用于区分资源是原贴还是评论（if =0，则为原帖；if=某资源id，则为评论）

    @Column(name = "recognition")
    private Integer recognition; //点赞数

    @Column(name = "opposition")
    private Integer opposition; //反对数

    @Column(name = "pageview")
    private Integer pageview; //浏览数

    @Column(name = "collection")
    private Integer collection; //收藏数

    @Column(name = "group_id", columnDefinition = "TEXT")
    private String groupId; //资源所属用户组id

    @Column(name = "label_id", columnDefinition = "TEXT")
    private String labelId; //资源的标签id

    @Column(name = "permissionId")
    private Integer permissionId; //资源是否为公开资源（0为非公开，1为公开）

    @Column(name = "contents", columnDefinition = "TEXT")
    private String contents; //资源书籍目录

    @Transient
    private String groupName; //用户组名称

    @Transient
    private String labelName; //资源标签名称

    @Transient
    private String userName; //用户名

    @Transient
    private Integer specType; //推荐

    @Transient
    private Integer specOrder; //推荐排序

    @Transient
    private Integer recommendType; //推荐

    @Transient
    private Integer recommendOrder; //推荐排序

    @Transient
    private Integer noticeType; //公告

    @Transient
    private Integer noticeOrder; //公告排序

    @Transient
    private Integer searchedByAttachment; //是否由附件字段命中

    @Transient
    private Integer searchedByText; //是否由正文字段命中

    @Transient
    private Integer searchedByComment; //是否由评论字段命中

    public Resource(int id) {
        this.id = id;
    }

}
