package com.free4inno.knowledgems.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import java.sql.Timestamp;

/**
 * Author HUYUZHU.
 * Date 2021/3/19 13:39.
 */

@Entity
@Table(name = "attachment")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Attachment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id; //主键

    @Column(name = "resourceId")
    private Integer resourceId; //附件所属资源Id

    @Column(name = "createTime")
    private Timestamp createTime; //上传时间

    @Column(name = "name")
    private String name; //名称

    @Column(name = "url")
    private String url; //地址

    @Column(name = "text", columnDefinition = "LONGTEXT")
    private String text; //解析文本

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private AttachmentEnum status; //解析状态

    @Column(name = "overTime")
    private Timestamp overTime; //解析结束时间

    /**
     * 状态.
     */
    public enum AttachmentEnum {
        READY, LOADING, SUCCESS, FAILED,UNSUPPORTED;

        public String toString() {
            switch (this) {
                case READY:
                    return "ready";
                case LOADING:
                    return "loading";
                case SUCCESS:
                    return "success";
                case FAILED:
                    return "failed";
                case UNSUPPORTED:
                    return "unsupported";
            }
            return super.toString();
        }
    }

    public enum AttachmentUpdate {
        UPDATE_SCHEDULE_BODY;
        public String toString() {
            if (this == AttachmentUpdate.UPDATE_SCHEDULE_BODY) {
                return"{\"query\":{\"nested\":{\"path\":\"attachment\",\"query\":{\"bool\":{\"must_not\":[{\"match\":{\"attachment.status\":\"SUCCESS\"}}]}}}}}";
            }
            return super.toString();
        }
    }

}
