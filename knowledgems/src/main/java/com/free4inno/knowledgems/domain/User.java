package com.free4inno.knowledgems.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Author HaoYi.
 * Date 2020/9/11.
 */
@Entity
@Table(name = "user")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private Integer id; //主键，用户Id

    @Column(name = "crop_id")
    private Integer cropId; //预留字段

    @Column(name = "account")
    private String account; //账户（手机号）

    @Column(name = "user_password")
    private String userPassword; //用户密码

    @Column(name = "real_name")
    private String realName; //真实姓名

    @Column(name = "mail")
    private String mail; //邮箱

    @Column(name = "account_name")
    private String accountName; //账户名（昵称）

    @Column(name = "role_id")
    private Integer roleId; //用户类型：0普通，1超级管理员，2用户管理员，3内容管理员，4系统管理员

    @Column(name = "app_key")
    private String appKey;

    public User(int id) {
        this.id = id;
    }
}
