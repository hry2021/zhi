package com.free4inno.knowledgems.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import java.sql.Timestamp;


/**
 * Author HaoYi.
 * Date 2020/9/11.
 */
@Entity
@Table(name = "group_info")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class GroupInfo {

    @Transient
    private int memberNum;

    @Transient
    private String leader;

    @Transient
    private int resourceNum;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "group_id")
    private Integer id; //主键

    @Column(name = "crop_id")
    private Integer cropId; //CropId

    @Column(name = "group_name")
    private String groupName; //用户组名

    @Column(name = "group_info", columnDefinition = "TEXT")
    private String groupInfo; //用户组描述

    @Column(name = "create_time")
    private Timestamp createTime; //资源创建时间

    @Column(name = "edit_time")
    private Timestamp editTime; //资源修改时间

    public GroupInfo(int id) {
        this.id = id;
    }
}
