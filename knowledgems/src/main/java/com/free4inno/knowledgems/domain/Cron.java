package com.free4inno.knowledgems.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Cron.
 */

@Entity
@Table(name = "cron")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Cron {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "cron_id")
    private Integer cronId; //主键

    @Column(name = "cron")
    private String cron; //资源ID

    @Column(name = "tikaCron")
    private String tikaCron;

}
