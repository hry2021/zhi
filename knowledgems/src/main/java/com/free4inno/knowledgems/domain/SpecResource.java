package com.free4inno.knowledgems.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import java.sql.Timestamp;

/**
 * Author: HaoYi.
 * Date: 2021/02/07.
 */
@Entity
@Table(name = "spec_resource")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SpecResource {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;  //主键

    @Column(name = "resource_id")
    private Integer resourceId;  //资源ID

    @Column(name = "spec_order")
    private Integer order;  //资源排序

    @Column(name = "type")
    private Integer type;  //1表示推荐，2表示公告

    @Column(name = "insert_time")
    private Timestamp insert_time;  //1表示推荐，2表示公告

    public SpecResource(int id) {
        this.id = id;
    }
}
