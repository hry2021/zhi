package com.free4inno.knowledgems.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import java.sql.Timestamp;

/**
 * Author HaoYi.
 * Date 2020/9/11.
 */
@Entity
@Table(name = "user_group")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserGroup {
    @Transient
    private String groupName;
    @Transient
    private String realName;
    @Transient
    private String account;
    @Transient
    private Timestamp createTime;
    @Transient
    private String groupInfo;
    @Transient
    private String leader;
    @Transient
    private int memberNum;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id; //主键

    @Column(name = "crop_id")
    private Integer cropId; //CropId

    @Column(name = "user_id")
    private Integer userId; //用户Id

    @Column(name = "group_id")
    private Integer groupId; //用户组Id

    @Column(name = "permission")
    private Integer permission; //权限，1为群主，2为管理员，0为成员

    public UserGroup(int id) {
        this.id = id;
    }
}
