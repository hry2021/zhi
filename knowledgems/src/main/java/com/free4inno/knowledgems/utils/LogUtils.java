package com.free4inno.knowledgems.utils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class LogUtils {
    public static String parse(String event, String message) {
        return String.format("{\"event\": \"%s\", \"message\": \"%s\"}", event, message);
    }
}
