<%--
  Created by IntelliJ IDEA.
  User: Creed
  Date: 2020/9/14
  Time: 13:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!--
START: Navbar

Additional Classes:
.dx-navbar-sticky || .dx-navbar-fixed
.dx-navbar-autohide
.dx-navbar-dropdown-triangle
.dx-navbar-dropdown-dark - only <ul>
.dx-navbar-expand || .dx-navbar-expand-lg || .dx-navbar-expand-xl
-->
<nav class="navbar navbar-top navbar-collapse navbar-sticky navbar-expand-lg navbar-dropdown-triangle navbar-autohide">
    <div class="container">

        <a href="../.." class="navbar-logo">
            <img src="../../assets/images/logo.png" alt="" width="88px">
        </a>

        <button class="navbar-burger" onclick="openit()">
            <span></span><span></span><span></span>
        </button>

        <div class="navbar-content">
            <ul class="nav nav-align-left">
                <li class="navbar-drop-item ${param.menu=='index'?'active':''}">
                    <a href="../.." name="index">
                        首页
                    </a>
                </li>
                <li class="navbar-drop-item ${param.menu=='search'?'active':''}">
                    <a href="../adsearch/adsearch" name="search">
                        搜索
                    </a>
                </li>
<%--                <li class="navbar-drop-item ${param.menu=='label'?'active':''}">--%>
<%--                    <a href="../labels/labels?id=" name="label">--%>
<%--                        分类--%>
<%--                    </a>--%>
<%--                </li>--%>
                <li class="navbar-drop-item ${param.menu=='group'?'active':''}">
                    <a href="<%=request.getContextPath()%>/group/group" name="group">
                        群组
                    </a>
                </li>
                <li class="navbar-drop-item ${param.menu=='newres'?'active':''}">
                    <a href="../crudres/new" name="newres">
                        新建
                    </a>
                </li>
                <c:if test="${sessionScope != null && sessionScope.login_flag == 1}">
                    <c:if test="${(sessionScope.roleId == 1) or (sessionScope.roleId == 2) or (sessionScope.roleId == 3)}">
                        <li class="navbar-drop-item ${param.menu=='manage'?'active':''}">
                            <a href="../../manage" name="manage">
                                管理
                            </a>
                        </li>
                    </c:if>
                </c:if>
<%--                                <li class="dx-drop-item">--%>
<%--                                    <a href="#">--%>
<%--                                        多级菜单示例--%>
<%--                                    </a>--%>
<%--                                    <ul class="dx-navbar-dropdown">--%>
<%--                                        <li>--%>
<%--                                            <a href="#">--%>
<%--                                                一级--%>
<%--                                            </a>--%>
<%--                                        </li>--%>
<%--                                        <li class="dx-drop-item">--%>
<%--                                            <a href="#">--%>
<%--                                                二级--%>
<%--                                            </a>--%>
<%--                                            <ul class="dx-navbar-dropdown">--%>
<%--                                                <li class="active">--%>
<%--                                                    <a href="#">--%>
<%--                                                        二级1--%>
<%--                                                    </a>--%>
<%--                                                </li>--%>
<%--                                                <li>--%>
<%--                                                    <a href="#">--%>
<%--                                                        二级2--%>
<%--                                                    </a>--%>
<%--                                                </li>--%>
<%--                                            </ul>--%>
<%--                                        </li>--%>
<%--                                    </ul>--%>
<%--                                </li>--%>
            </ul>

            <ul class="nav nav-align-right">
                <c:choose>
                    <c:when test="${sessionScope == null or sessionScope.login_flag == 0}">
                        <li>
                            <a href="../../login">登录</a>
                        </li>
                    </c:when>
                    <c:otherwise>
                        <li class="navbar-drop-item">
                            <c:choose>
                                <c:when test="${sessionScope.userName == null or sessionScope.userName.length() < 1}">
                                    <li>
                                        <a href="../../login">登录</a>
                                    </li>
                                </c:when>
                                <c:otherwise>
                                    <a href="#" id="user-name">${sessionScope.userName}</a>
                                </c:otherwise>
                            </c:choose>

                            <ul class="navbar-dropdown">
                                <li>
                                    <a id="psw-reset-button1" onclick="showChangePasswordModal(${sessionScope.userId})" href="#">修改密码</a>
                                </li>
                                <li>
                                    <a id="personal-data-button1" onclick="showUserInfoModal('${sessionScope.userName}','${sessionScope.account}','${sessionScope.accountMail}',${sessionScope.roleId})" href="#">个人信息</a>
                                </li>
                                <li>
                                    <a id="myresource-data-button1" href="/myresource?userId=${sessionScope.userId}">我的</a>
                                </li>
                                <li>
                                    <a id="log-out-button1" href="#" onclick="logout()">退出</a>
                                </li>
                            </ul>
                        </li>
                    </c:otherwise>
                </c:choose>
<%--                <c:choose>--%>
<%--                    <c:when test="${sessionScope.login_flag == 0}">--%>
<%--                        <li>--%>
<%--                            <span><a href="../login/signup.jsp" class="dx-btn dx-btn-md dx-btn-transparent">注册</a></span>--%>
<%--                        </li>--%>
<%--                    </c:when>--%>
<%--                </c:choose>--%>
            </ul>
        </div>
    </div>
</nav>
<div class="navbar navbar-fullscreen" id="box">
    <div class="container">
        <button class="navbar-burger active" style="top:30px" onclick="hiddenit()">
            <span></span><span></span><span></span>
        </button>
        <div class="navbar-content" style="text-align: center">
            <ul class="nav nav-align-left">
                <li class="${param.menu=='index'?'active':''}">
                    <a href="../..">首页</a>
                </li>
                <li class="${param.menu=='search'?'active':''}">
                    <a href="../adsearch/adsearch">搜索</a>
                </li>
                <li class="${param.menu=='label'?'active':''}">
                    <a href="../labels/labels?id=">分类</a>
                </li>
                <li class="${param.menu=='group'?'active':''}">
                    <a href="<%=request.getContextPath()%>/group/group">群组</a>
                </li>
                <li class="${param.menu=='newres'?'active':''}">
                    <a href="../crudres/new#">新建</a>
                </li>
                <c:if test="${sessionScope.login_flag == 1}">
                    <c:if test="${(sessionScope.roleId == 1) or (sessionScope.roleId == 2) or (sessionScope.roleId == 3)}">
                        <li class="${param.menu=='manage'?'active':''}">
                            <a href="../../manage">管理</a>
                        </li>
                    </c:if>
                </c:if>
            </ul>
                <%--                <li class="dx-drop-item">--%>
                <%--                    <a href="#account.html">--%>
                <%--                        多级菜单示例--%>
                <%--                    </a>--%>
                <%--                    <ul class="dx-navbar-dropdown">--%>
                <%--                        <li>--%>
                <%--                            <a href="#">--%>
                <%--                                一级--%>
                <%--                            </a>--%>
                <%--                        </li>--%>
                <%--                        <li class="dx-drop-item">--%>
                <%--                            <a href="#">--%>
                <%--                                二级--%>
                <%--                            </a>--%>
                <%--                            <ul class="dx-navbar-dropdown">--%>
                <%--                                <li class=" active">--%>
                <%--                                    <a href="#">--%>
                <%--                                        二级1--%>
                <%--                                    </a>--%>
                <%--                                </li>--%>
                <%--                                <li>--%>
                <%--                                    <a href="#">--%>
                <%--                                        二级2--%>
                <%--                                    </a>--%>
                <%--                                </li>--%>
                <%--                            </ul>--%>
                <%--                        </li>--%>
                <%--                    </ul>--%>
                <%--                </li>--%>
                <%--            </ul>--%>

            <ul class="nav nav-align-right" style="display: inline-block">
                <c:choose>
                    <c:when test="${sessionScope != null && sessionScope.login_flag == 1}">
                        <li class="navbar-drop-item">
                            <a href="#" style="padding-right: 0px;">${sessionScope.userName}</a>
                            <ul class="navbar-dropdown" style="margin-right: 0px;padding-left: 0px;">
                                <li>
                                    <a id="psw-reset-button2" onclick="showChangePasswordModal(${sessionScope.userId})" href="#">修改密码</a>
                                </li>
                                <li>
                                    <a id="personal-data-button2" onclick="showUserInfoModal('${sessionScope.userName}','${sessionScope.account}','${sessionScope.accountMail}',${sessionScope.roleId})" href="#">个人信息</a>
                                </li>
                                <li>
                                    <a id="myresource-data-button2" href="/myresource?userId=${sessionScope.userId}">我的</a>
                                </li>
                                <li>
                                    <a id="log-out-button2" href="#" onclick="logout()">退出</a>
                                </li>
                            </ul>
                        </li>
                    </c:when>
                    <c:otherwise>
                        <li>
                            <a href="/login">登录</a>
                        </li>
                    </c:otherwise>
                </c:choose>
<%--                <c:choose>--%>
<%--                    <c:when test="${sessionScope.login_flag == 0}">--%>
<%--                        <li>--%>
<%--                            <span><a href="../login/signup.jsp" class="dx-btn dx-btn-md dx-btn-transparent">注册</a></span>--%>
<%--                        </li>--%>
<%--                    </c:when>--%>
<%--                </c:choose>--%>
            </ul>
        </div>
    </div>
</div>
<div class="modal fade" id="changePasswordModal" tabindex="-1" role="dialog" aria-labelledby="changePasswordModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modifyPassword">修改密码</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="col-lg-7">
                        <label>请输入原密码</label>
                        <input type="password" class="form-control front-no-box-shadow" name="applicationDes" id="oldPassword" placeholder="必填"/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-7">
                        <label>请输入新密码</label>
                        <input type="password" class="form-control front-no-box-shadow" name="applicationDes" id="newPassword" placeholder="必填"/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-7">
                        <label>请再次输入</label>
                        <input type="password" class="form-control front-no-box-shadow" name="applicationDes" id="confirmPassword" placeholder="必填"/>
                    </div>
                </div>
                <div class="form-group" style="display: none">
                    <div class="col-lg-7">
                        <label>ID</label>
                        <input type="text" class="form-control front-no-box-shadow" name="applicationDes" id="userId" placeholder="必填"/>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="modify_btn_add" class="btn btn-primary" data-dismiss="modal"
                        onclick="confirmChangePassword()"><a>确定</a></button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="addSamePassword" tabindex="-1" role="dialog" aria-labelledby="addMemberLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="same_header">无法执行此操作</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="col-lg-7">
                        <label>两次密码不一致，请重新输入！</label>
                        <input type="text" class="form-control front-no-box-shadow" name="applicationDes" id="tagIdSame" style="display:none;"/>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="cancelSame" class="btn btn-primary" data-dismiss="modal"
                        onclick=""><a>确定</a></button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="userInfo" tabindex="-1" role="dialog" aria-labelledby="addMemberLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modifyUserLabel">个人信息</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="col-lg-7">
                        <label>姓名</label>
                        <input type="text" class="form-control front-no-box-shadow" name="applicationDes" id="userName" readonly/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-7">
                        <label>手机</label>
                        <input type="text" class="form-control front-no-box-shadow" name="applicationDes" id="userAccount" readonly/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-7">
                        <label>邮箱</label>
                        <input type="text" class="form-control front-no-box-shadow" name="applicationDes" id="userMail" readonly/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-7">
                        <label>角色</label>
                        <input type="text" class="form-control front-no-box-shadow" name="applicationDes" id="userRole" readonly/>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="user_info_btn" class="btn btn-primary" data-dismiss="modal"
                        onclick=""><a>确定</a></button>
            </div>
        </div>
    </div>
</div>
<!-- END: Navbar -->
<script type="text/javascript">
    function openit(){
        $("#box").css("visibility", "visible");
        $("#box").css("opacity", "1");
        $("#box").css("z-index", "1000");
    }
    function hiddenit(){
        $("#box").css("visibility", "hidden");
        $("#box").css("opacity", "0");
        $("#box").css("z-index", "-1000");
    }
</script>
