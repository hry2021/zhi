<%--
  Created by IntelliJ IDEA.
  User: Creed
  Date: 2021/1/15
  Time: 20:37
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
    <title>templateList</title>
</head>
<body>

    <div class="row mb-9">
        <div id= "searchTemplateNum"  style="width: 70%; vertical-align: center">
            <a style="bottom: -9px; text-align: center; vertical-align: center; margin-left: 15px; padding-top: 30px; margin-top: 30px" id="searchedText">共有${searchedTemplateNum}个模板</a>
        </div>
        <div id="appManage" align="right" style="float: right; width: 22%">
            <input type="text" class="form-control" id="searchBox" placeholder="搜索模板" autofocus="autofocus">
        </div>
        <div align="right" style="float: right; width: 8%">
            <a class="btn btn-primary pull-right" type="button" href="../template/newTemplate.jsp" style="margin-right: 16px"></span>新建</a>
        </div>
    </div>



<div id="dataList" class="panel panel-default front-panel">
    <table id="infoTable" class="table table-striped front-table table-bordered" style="margin-bottom: 0px">
        <thead>
        <tr>
            <th style="text-align: center">名称</th>
<%--            <th style="text-align: center">templateCode</th>--%>
            <th style="text-align: center">创建时间</th>
<%--            <th style="text-align: center">setTime</th>--%>
            <th style="text-align: center">操作</th>
        </tr>
        </thead>
        <c:forEach var="template" items="${templateList}">
            <tr id="${group.id}">
                <td style="vertical-align: middle;overflow: auto;text-align: center">${template.templateName}</td>
<%--                <td style="vertical-align: middle;overflow: auto;text-align: center">${template.templateCode}</td>--%>
                <td style="vertical-align: middle;overflow: auto;text-align: center">${template.createTime}</td>
<%--                <td style="vertical-align: middle;overflow: auto;text-align: center">${template.setTime}</td>--%>
                <td style="vertical-align: middle;overflow: auto;text-align: center">
                    <a href="javascript:void(0)" onclick="window.open('<%=request.getContextPath()%>/template/templateContent?templateId=${template.id}','_self');">详情</a>
<%--                    <a href="javascript:void(0)" onclick="window.open('<%=request.getContextPath()%>/groupinfo?groupid=${template.id}','_self');">修改</a>--%>
                    <a href="javascript:void(0)" onclick="confirmDeleteTemplate(${template.id})">删除</a>

                </td>
            </tr>
        </c:forEach>
    </table>


</div>
    <div id="pageTemplateNumbers" class="text-center"></div>
</body>

<script>
    showPageNumbers();
    function showPageNumbers(){
        var htmlstr = "";
        var page = parseInt("${currentPage}");
        var maxPage = parseInt("${maxPages}");
        console.log("page:"+page+",maxPage:"+maxPage);
        var maxShowPage = 10;
        if(maxShowPage > maxPage) maxShowPage = maxPage;
        // if(maxPage <= 1){
        //     console.log("maxPage <= 1");
        //     return;
        // }
        htmlstr += "<div class=\"text-center\">" +
            "<ul class=\"re-pagination mt-40\">";
        if(page <= 1){
            htmlstr += "<li class=\"re-pagination-icon disabled\"><a aria-label=\"Previous\"><span class=\"icon pe-7s-angle-left\"></span></a></li>";
        }
        else{
            htmlstr += "<li class=\"re-pagination-icon disabled\"><a href=\"javascript:getAppPage("+ (page-1).toString() + ");\" aria-label=\"Previous\"><span class=\"icon pe-7s-angle-left\"></span></a></li>";
        }

        if(maxPage <= maxShowPage){
            for(i = 1; i <= maxShowPage; i++){
                if(i === page){
                    htmlstr += "<li class=\"active\"><a>"+ i.toString() +"</a></li>";
                }
                else{
                    htmlstr += "<li><a href=\"javascript:getAppPage("+ i.toString() +");\">"+ i.toString() +"</a></li>";
                }
            }
        }
        else{
            if(page < maxShowPage - 2){
                for(i = 1; i <= maxShowPage - 2; i++){
                    if(i === page){
                        htmlstr += "<li class=\"active\"><a>"+ i.toString() +"</a></li>";
                    }
                    else{
                        htmlstr += "<li><a href=\"javascript:getAppPage("+ i.toString() +");\">"+ i.toString() +"</a></li>";
                    }
                }
                htmlstr += "<li><a aria-label=\"AfterMore\">...</a></li>";
                htmlstr += "<li><a href=\"javascript:getAppPage("+maxPage.toString()+");\" aria-label=\"Last\">"+maxPage.toString()+"</a></li>";
            }
            else if(page >= maxShowPage - 2 && page <= maxPage - maxShowPage + 3){
                htmlstr += "<li><a href=\"javascript:getAppPage(1);\">1</a></li>";
                htmlstr += "<li><a aria-label=\"AfterMore\">...</a></li>";
                htmlstr += "<li><a href=\"javascript:getAppPage("+ (page-2).toString() +");\">"+ (page-2).toString() +"</a></li>";
                htmlstr += "<li><a href=\"javascript:getAppPage("+ (page-1).toString() +");\">"+ (page-1).toString() +"</a></li>";
                htmlstr += "<li class=\"active\"><a>"+ page.toString() +"</a></li>";
                for(i = page + 1; i <= maxShowPage - 7 + page; i++){
                    htmlstr += "<li><a href=\"javascript:getAppPage("+ i.toString() +");\">"+ i.toString() +"</a></li>";
                }
                htmlstr += "<li><a aria-label=\"AfterMore\">...</a></li>";
                htmlstr += "<li><a href=\"javascript:getAppPage("+maxPage.toString()+");\" aria-label=\"Last\">"+maxPage.toString()+"</a></li>";
            }
            else{
                htmlstr += "<li><a href=\"javascript:getAppPage(1);\">1</a></li>";
                htmlstr += "<li><a aria-label=\"AfterMore\">...</a></li>";
                for(i = maxPage - maxShowPage + 3; i <= maxPage; i++){
                    if(i === page){
                        htmlstr += "<li class=\"active\"><a>"+ i.toString() +"</a></li>";
                    }
                    else{
                        htmlstr += "<li><a href=\"javascript:getAppPage("+ i.toString() +");\">"+ i.toString() +"</a></li>";
                    }
                }
            }
        }
        if(parseInt(page) >= maxPage){
            htmlstr += "<li class=\"re-pagination-icon disabled\"><a aria-label=\"Next\"><span class=\"icon pe-7s-angle-right\"></span></a></li>";
        }
        else{
            htmlstr += "<li class=\"re-pagination-icon disabled\"><a href=\"javascript:getAppPage("+ (parseInt(page)+1).toString() + ");\" aria-label=\"Next\"><span class=\"icon pe-7s-angle-right\"></span></a></li>";
        }
        htmlstr += "</ul>" +
            "</div>"
        $("#pageTemplateNumbers").html(htmlstr);
    }
</script>
</html>
