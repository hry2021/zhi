<%--
  Created by IntelliJ IDEA.
  User: liyunze
  Date: 2021/1/29
  Time: 下午11:31
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>userList</title>
</head>
<body>
    <div class="row" style="margin-top: 15px">
        <div style="margin-left: 21px;margin-right: auto; width: 55%">
            <ul class="re-btn-manage re-btn-manage-style">
                <li style="margin: 10px -1px 0">
                    <a href="#" style="height: 39px; border-radius: 0" class="re-btn-manage-cont" onclick="showUsers(1,'')">用户</a>
                </li>
                <li style="margin: 10px -1px 0">
                    <a href="#" style="height: 39px; border-radius: 0" class="re-btn-manage-cont" onclick="showManager(1,'')">管理员</a>
                </li>
                <li style="margin: 10px -1px 0">
                    <a href="#" style="height: 39px; background-color: white; border-radius: 0" class="re-btn-manage-cont" onclick="showApply(1,'')">用户注册请求 ( ${applyNum} ) </a>
                </li>
            </ul>
        </div>
        <div id="appManage" style="margin-right: 15px;width: 22%;">
            <input type="text" class="form-control" id="searchBoxApply" placeholder="搜索新用户请求" autofocus="autofocus">
        </div>
    </div>
    <div id="dataList" class="panel panel-default front-panel" style="margin-top:10px">
    <table id="infoTable" class="table table-striped front-table table-bordered" style="margin-bottom: 0px">
        <thead>
        <tr>
            <th style="text-align: center">姓名</th>
            <th style="text-align: center">手机</th>
            <th style="text-align: center">邮箱</th>
            <th style="text-align: center;width: 240px">时间</th>
            <th style="text-align: center">申请理由</th>
            <th style="text-align: center;width: 120px">操作</th>
        </tr>
        </thead>
        <c:forEach var="apply" items="${applyList}">
            <tr id="${apply.id}">
                <td style="vertical-align: middle;overflow: auto;text-align: center">${apply.name}</td>
                <td style="vertical-align: middle;overflow: auto;text-align: center">${apply.telnumber}</td>
                <td style="vertical-align: middle;overflow: auto;text-align: center">${apply.mail}</td>
                <td style="vertical-align: middle;overflow: auto;text-align: center">
                    <div style="margin-bottom: 5px">申请时间：<fmt:formatDate value="${apply.signupTime}" pattern="yyyy-MM-dd HH:mm"/></div>
                    <div id="time${apply.id}">拒绝时间：<fmt:formatDate value="${apply.processTime}" pattern="yyyy-MM-dd HH:mm"/></div>
                </td>
                <td style="vertical-align: middle;overflow: auto;text-align: center">${apply.reason}</td>
                <td style="vertical-align: middle;overflow: auto;text-align: center">
                    <div id="operation${apply.id}"></div>
                    <script>
                        showoperation();
                        function showoperation(){
                            var Status = ${apply.status};
                            var operationhtmlstr = "";
                            if(Status === 0){
                                $("#time${apply.id}").hide();
                                operationhtmlstr = "<div><a href=\"javascript:void(0)\" onclick=\"applyToUser(${apply.id},${apply.telnumber},0)\">通过</a><a> </a>" +
                                "<a href=\"javascript:void(0)\" onclick=\"applyToNo(${apply.id})\">拒绝</a></div><div><a href=\"javascript:void(0)\" onclick=\"applyToUser(${apply.id},${apply.telnumber},1)\">加入访客</a></div>";
                            }else if(Status === 2){
                                operationhtmlstr = "<a href=\"javascript:void(0)\" onclick=\"deleteApply(${apply.id})\">删除</a>";
                            }
                            $("#operation${apply.id}").html(operationhtmlstr);
                        }
                    </script>
                </td>
            </tr>
        </c:forEach>
    </table>
</div>
    <div id="applyPageNumbers" class="text-center"></div>
</body>
<script>
    showPageNumbers();
    function showPageNumbers(){
        var htmlstr = "";
        var page = parseInt("${currentPage}");
        var maxPage = parseInt("${maxPages}");
        var maxShowPage = 10;
        if(maxShowPage > maxPage) maxShowPage = maxPage;
        if(maxPage <= 1){
            return;
        }
        htmlstr += "<div class=\"text-center\">" +
            "<ul class=\"re-pagination mt-40\">";
        if(page <= 1){
            htmlstr += "<li class=\"re-pagination-icon disabled\"><a aria-label=\"Previous\"><span class=\"icon pe-7s-angle-left\"></span></a></li>";
        }
        else{
            htmlstr += "<li class=\"re-pagination-icon disabled\"><a href=\"javascript:getAppPage("+ (page-1).toString() + ");\" aria-label=\"Previous\"><span class=\"icon pe-7s-angle-left\"></span></a></li>";
        }

        if(maxPage <= maxShowPage){
            for(i = 1; i <= maxShowPage; i++){
                if(i === page){
                    htmlstr += "<li class=\"active\"><a>"+ i.toString() +"</a></li>";
                }
                else{
                    htmlstr += "<li><a href=\"javascript:getAppPage("+ i.toString() +");\">"+ i.toString() +"</a></li>";
                }
            }
        }
        else{
            if(page < maxShowPage - 2){
                for(i = 1; i <= maxShowPage - 2; i++){
                    if(i === page){
                        htmlstr += "<li class=\"active\"><a>"+ i.toString() +"</a></li>";
                    }
                    else{
                        htmlstr += "<li><a href=\"javascript:getAppPage("+ i.toString() +");\">"+ i.toString() +"</a></li>";
                    }
                }
                htmlstr += "<li><a aria-label=\"AfterMore\">...</a></li>";
                htmlstr += "<li><a href=\"javascript:getAppPage("+maxPage.toString()+");\" aria-label=\"Last\">"+maxPage.toString()+"</a></li>";
            }
            else if(page >= maxShowPage - 2 && page <= maxPage - maxShowPage + 3){
                htmlstr += "<li><a href=\"javascript:getAppPage(1);\">1</a></li>";
                htmlstr += "<li><a aria-label=\"AfterMore\">...</a></li>";
                htmlstr += "<li><a href=\"javascript:getAppPage("+ (page-2).toString() +");\">"+ (page-2).toString() +"</a></li>";
                htmlstr += "<li><a href=\"javascript:getAppPage("+ (page-1).toString() +");\">"+ (page-1).toString() +"</a></li>";
                htmlstr += "<li class=\"active\"><a>"+ page.toString() +"</a></li>";
                for(i = page + 1; i <= maxShowPage - 7 + page; i++){
                    htmlstr += "<li><a href=\"javascript:getAppPage("+ i.toString() +");\">"+ i.toString() +"</a></li>";
                }
                htmlstr += "<li><a aria-label=\"AfterMore\">...</a></li>";
                htmlstr += "<li><a href=\"javascript:getAppPage("+maxPage.toString()+");\" aria-label=\"Last\">"+maxPage.toString()+"</a></li>";
            }
            else{
                htmlstr += "<li><a href=\"javascript:getAppPage(1);\">1</a></li>";
                htmlstr += "<li><a aria-label=\"AfterMore\">...</a></li>";
                for(i = maxPage - maxShowPage + 3; i <= maxPage; i++){
                    if(i === page){
                        htmlstr += "<li class=\"active\"><a>"+ i.toString() +"</a></li>";
                    }
                    else{
                        htmlstr += "<li><a href=\"javascript:getAppPage("+ i.toString() +");\">"+ i.toString() +"</a></li>";
                    }
                }
            }
        }
        if(parseInt(page) >= maxPage){
            htmlstr += "<li class=\"re-pagination-icon disabled\"><a aria-label=\"Next\"><span class=\"icon pe-7s-angle-right\"></span></a></li>";
        }
        else{
            htmlstr += "<li class=\"re-pagination-icon disabled\"><a href=\"javascript:getAppPage("+ (parseInt(page)+1).toString() + ");\" aria-label=\"Next\"><span class=\"icon pe-7s-angle-right\"></span></a></li>";
        }
        htmlstr += "</ul>" +
            "</div>"
        $("#applyPageNumbers").html(htmlstr);
    }
</script>
</html>