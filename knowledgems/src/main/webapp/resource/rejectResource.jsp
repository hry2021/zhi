<%--
  Created by IntelliJ IDEA.
  User: HUYUZHU
  Date: 2020/9/4
  Time: 15:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt_rt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <c:import url="../template/_header.jsp"/>
    <title>${resource.title}-知了[团队知识管理应用]</title>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
</head>

<body>
<div class="re-main">
    <c:import url="../template/_navbar.jsp"/>
    <div class="re-box-6 mt-85">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="re-box re-box-decorated resource-post">
                        <div class="resource-post-box">
                            <%-------------------- 资源标题 --------------------%>
                            <h1 class="h3 resource-post-title">${resource.title}</h1>
                            <%-------------------- 资源标签 --------------------%>
                            <c:if test="${!resource.labelName.equals(\"该资源无标签\")}">
                                <ul class="resource-post-info">
                                    <li class="font-text-darkgray">标签:&nbsp;</li>
                                    <c:forEach var="label" items="${resource.labelName.split(',|，')}" varStatus="s">
                                        <c:forEach var="labelId" items="${resource.labelId.split(',|，')}" varStatus="gi">
                                            <c:if test="${s.index==gi.index}">
                                                <li><a class="font-text font-text-darkgray" href="../labels/labels?id=${labelId.trim()}">${label.trim()}</a></li>
                                            </c:if>
                                        </c:forEach>
                                    </c:forEach>
                                </ul>
                            </c:if>
                            <%-------------------- 资源群组 --------------------%>

                            <c:if test="${!resource.groupName.equals(\"该资源无群组\")}">
                                <ul class="resource-post-info col-md-4">

                                    <c:forEach var="groupName" items="${resource.groupName.split(',|，')}"
                                               varStatus="gn">
                                        <c:forEach var="groupId" items="${resource.groupId.split(',|，')}"
                                                   varStatus="gi">
                                            <c:choose>
                                                <c:when test="${gi.index==gn.index}">
                                                    <c:if test="${groupId.trim() != 0 && groupId.trim() != 1}">
                                                        <li class="font-text-darkgray">群组:&nbsp;</li>
                                                        <li><a class="font-text-darkgray"
                                                               href="../groupinfo?groupid=${groupId.trim()}">${groupName.trim()}</a>
                                                        </li>
                                                    </c:if>
                                                </c:when>
                                            </c:choose>
                                        </c:forEach>
                                    </c:forEach>
                                </ul>
                            </c:if>
                            <%-------------------- 资源作者 --------------------%>
                            <ul class="resource-post-info">
                                <li class="font-text-darkgray">作者:&nbsp;</li>
                                <li><a href="/myresource?userId=${resource.userId}" class="font-text-darkgray">${user.realName}</a></li>
                            </ul>
                            <%-------------------- 资源时间 --------------------%>
                            <ul class="resource-post-info">
                                <li class="font-text-darkgray">时间:&nbsp;</li>
                                <li class="font-text-darkgray"><fmt:formatDate pattern="yyyy-MM-dd HH:mm" value="${resource.createTime}"/></li>
                            </ul>
                        </div>
                        <%-------------------- 资源正文 --------------------%>
                        <div class="resource-post-box resource-post-article">
                            （抱歉！您无权查看当前资源）
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- START: Footer -->
    <c:import url="../template/_footer.jsp"/>
    <!-- END: Footer -->
</div>


</body>
<!-- START: Scripts -->
<c:import url="../template/_include_js.jsp"/>

<script language="JavaScript">
    window.onload = (function () {
        $.fillTipBox({type: 'danger', icon: 'glyphicon-alert', content: '抱歉！您无权查看该资源'});
    });
</script>
<!-- END: Scripts -->
</html>