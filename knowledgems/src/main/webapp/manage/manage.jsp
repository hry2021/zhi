<%--
  Created by IntelliJ IDEA.
  User: HUYUZHU
  Date: 2021/1/16
  Time: 20:39
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <c:import url="../template/_header.jsp"/>
    <title>管理-知了[团队知识管理应用]</title>
</head>

<body>
<div class="re-main">
    <c:import url="../template/_navbar.jsp?menu=manage"/>
    <div class="re-box-6 re-grey mt-85">
        <div class="container">
            <div class="row vertical-gap md-gap">
                <div class="col-lg-12">
                    <div class="row vertical-gap">
                        <%--  系统管理  --%>
                            <c:if test="${sessionScope.roleId==1 or sessionScope.roleId==4}">
                                <div class="col-12 col-md-6 col-lg-4"
                                     onclick="window.open('<%=request.getContextPath()%>/manage/systemManage','_self')">
                                    <div class="re-portfolio-item re-block-decorated">
                                        <div class="resource-item re-box re-box-decorated">
                                            <div class="resource-item-cont">
                                                <h3><a class="text-dark mb-30"
                                                                            href="<%=request.getContextPath()%>/manage/systemManage">系统管理</a>
                                                </h3>
                                                <ul class="resource-item-info">
                                                    <li>系统管理</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </c:if>
                        <%--  首页管理  --%>
                        <%--                            <div class="col-12 col-md-6 col-lg-4 dx-isotope-grid-item" onclick="window.open('#','_self')" style="cursor:pointer">--%>
                        <%--                                <div class="dx-portfolio-item dx-block-decorated">--%>
                        <%--                                    <div class="dx-blog-item dx-box dx-box-decorated">--%>
                        <%--                                        <div class="dx-blog-item-cont">--%>
                        <%--                                            <h3 style="color: black"><a style="color: #0a0a0a" href="#">首页管理</a></h3>--%>
                        <%--                                            <ul class="dx-blog-item-info">--%>
                        <%--                                                <li>xxxxx</li>--%>
                        <%--                                                <li>yyyyy</li>--%>
                        <%--                                            </ul>--%>
                        <%--                                        </div>--%>
                        <%--                                    </div>--%>
                        <%--                                </div>--%>
                        <%--                            </div>--%>
                        <%--  用户管理  --%>
                        <c:if test="${sessionScope.roleId==1 or sessionScope.roleId==2}">
                            <div class="col-12 col-md-6 col-lg-4 "
                                 onclick="window.open('<%=request.getContextPath()%>/user/userinfo','_self')">
                                <div class="re-portfolio-item re-block-decorated">
                                    <div class="resource-item re-box re-box-decorated">
                                        <div class="resource-item-cont">
                                            <h3><a class="text-dark mb-30" href="<%=request.getContextPath()%>/user/userinfo">用户管理</a>
                                            </h3>
                                            <ul class="resource-item-info">
                                                <li>用户：${userNum}</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </c:if>
                        <%--  群组管理  --%>
                        <c:if test="${sessionScope.roleId==1 or sessionScope.roleId==2}">
                            <div class="col-12 col-md-6 col-lg-4"
                                 onclick="window.open('<%=request.getContextPath()%>/groupmanage','_self')">
                                <div class="re-portfolio-item re-block-decorated">
                                    <div class="resource-item re-box re-box-decorated">
                                        <div class="resource-item-cont">
                                            <h3><a class="text-dark mb-30"
                                                                        href="<%=request.getContextPath()%>/groupmanage">群组管理</a>
                                            </h3>
                                            <ul class="resource-item-info">
                                                <li>群组：${groupNum}</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </c:if>
                        <%--  分类管理  --%>
                        <c:if test="${sessionScope.roleId==1 or sessionScope.roleId==3}">
                            <div class="col-12 col-md-6 col-lg-4"
                                 onclick="window.open('<%=request.getContextPath()%>/labels/labelManage','_self')">
                                <div class="re-portfolio-item re-block-decorated">
                                    <div class="resource-item re-box re-box-decorated">
                                        <div class="resource-item-cont">
                                            <h3><a class="text-dark mb-30"
                                                                        href="<%=request.getContextPath()%>/labels/labelManage">分类管理</a>
                                            </h3>
                                            <ul class="resource-item-info">
                                                <li>一级标签：${labelNum1}</li>
                                                <li>二级标签：${labelNum2}</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </c:if>
                        <%--  模板管理  --%>
                            <c:if test="${sessionScope.roleId==1 or sessionScope.roleId==3}">
                                <div class="col-12 col-md-6 col-lg-4"
                                     onclick="window.open('<%=request.getContextPath()%>/template/templateInfo','_self')">
                                    <div class="re-portfolio-item re-block-decorated">
                                        <div class="resource-item re-box re-box-decorated">
                                            <div class="resource-item-cont">
                                                <h3><a class="text-dark mb-30"
                                                       href="<%=request.getContextPath()%>/template/templateInfo">模板管理</a>
                                                </h3>
                                                <ul class="resource-item-info">
                                                    <li>模板：${templateNum}</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </c:if>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- START: Footer -->
    <c:import url="../template/_footer.jsp"/>
    <!-- END: Footer -->
</div>
</body>
<!-- START: Scripts -->
<c:import url="../template/_include_js.jsp"/>
<!-- END: Scripts -->
</html>
