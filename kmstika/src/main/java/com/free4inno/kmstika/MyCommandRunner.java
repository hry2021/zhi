//package com.example.kmstika;
//
//import com.example.kmstika.service.AttachmentTikaService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.CommandLineRunner;
//import org.springframework.stereotype.Component;
//
///**
// * Author HUYUZHU.
// * Date 2021/3/26 18:05.
// */
//
//@Component
//public class MyCommandRunner implements CommandLineRunner {
//
//    @Autowired
//    AttachmentTikaService service;
//
//    @Override
//    public void run(String... args) {
//        service.parseAll();
//        //执行完自动关闭项目
//        KmstikaApplication.context.close();
//    }
//
//}
